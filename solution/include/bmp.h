#ifndef BMP_H
#define BMP_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "input.h"
#pragma pack(push, 1)

struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

//const num for checking file
#define BF_TYPE_MAX 0x4D42
#define BI_BIT_NORM 24
#define BI_SIZE_NORM 40


/*  deserializer   */
enum read_status  {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_IMAGE,
    READ_ERROR_MEMORY_ALLOCATION
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
    WRITE_OK,
    WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );


#endif
