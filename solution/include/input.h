#ifndef IMG_H
#define IMG_H

#include  <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

bool new_img(struct image* img, uint64_t width, uint64_t height);

void clear_img(struct image* img);
#endif
