#ifndef TR_ROT_H
#define TR_ROT_H

#include "input.h"

struct image rotate( struct image const source );

#endif
