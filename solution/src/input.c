#include "input.h"

bool new_img(struct image* img, uint64_t width, uint64_t height){
  img->width = width;
  img->height = height;
  img->data = malloc(sizeof(struct pixel)*width*height);
  if(img->data) return true;
  else return false;
}

void clear_img(struct image* img){
    free(img->data);

}
