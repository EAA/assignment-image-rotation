#include "bmp.h"

static enum read_status set_img_data(struct image* img, FILE* in);
static struct bmp_header new_bmp_head(struct image const* img);
static uint32_t get_pxs(struct image const* img);

enum read_status from_bmp( FILE* in, struct image* img ){

    struct bmp_header* head = malloc(sizeof(struct bmp_header));
    if(!head) return READ_ERROR_MEMORY_ALLOCATION;

    enum read_status err_read;

    //reading header
    fread(head, sizeof(struct bmp_header), 1, in);

    if (head->bfType > BF_TYPE_MAX || head->bfType <= 0) return READ_INVALID_SIGNATURE;
    if (head->biBitCount != BI_BIT_NORM) return READ_INVALID_BITS;
    if (head->biSize != BI_SIZE_NORM) return READ_INVALID_HEADER;

    if(!new_img(img, head->biWidth, head->biHeight)) 
        return READ_ERROR_MEMORY_ALLOCATION;

    free(head);

    err_read = set_img_data(img, in);
    if(err_read != READ_OK) return err_read;

    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header head = new_bmp_head(img);
    size_t err_check;

    err_check = fwrite(&head, sizeof(struct bmp_header), 1, out);
    if (err_check != 1) return WRITE_ERROR;


    const size_t start = 0;

    for(uint32_t i = 0; i < img->height; i++) {

        err_check = fwrite(&(img->data[i*img->width]),sizeof(struct pixel), img->width, out);
        if (err_check != img->width) return WRITE_ERROR;

        err_check = fwrite(&start, 1, img->width % 4, out);
        if (err_check != img->width % 4) return WRITE_ERROR;
    }

    return WRITE_OK;
}


static enum read_status set_img_data(struct image* img, FILE* in){
    size_t err_check;
    for(uint32_t i = 0; i < img->height; i++) {
         err_check = fread(&(img->data[i*img->width]), sizeof(struct pixel), img->width, in);
         if(err_check != img->width) return READ_INVALID_IMAGE;

         fseek( in, (int64_t)(img->width)%4, SEEK_CUR);
    }
    return READ_OK;
}

static uint32_t get_pxs(struct image const* img){
    return img->height * img->width * sizeof(struct pixel) + (img->width % 4)*img->height;
}

static struct bmp_header new_bmp_head(struct image const* img){
    struct bmp_header head = {
        .bfType = 0x4D42,
        .bfileSize = sizeof(struct bmp_header) + get_pxs(img),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = get_pxs(img),
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
    return head;
}
