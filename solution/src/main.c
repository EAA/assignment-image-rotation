#include "main.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    //input_image
    struct image ii;

    //transformed_image
    struct image tr_i;

    FILE *f = fopen(argv[1], "rb");

    FILE *new_f = fopen(argv[2], "wb");

    from_bmp(f, &ii);

    tr_i = rotate(ii);

    to_bmp(new_f, &tr_i);

    fclose(f);
    fclose(new_f);

    clear_img(&ii);
    clear_img(&tr_i);

    return 0;
}
