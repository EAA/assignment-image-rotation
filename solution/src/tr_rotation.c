#include "tr_rotation.h"

struct image rotate( struct image const source ){
    struct image tr_i = {0};

    new_img(&tr_i, source.height, source.width);

    for (uint64_t i = 0; i < source.width; i++) {
        for (uint64_t j = 0; j < source.height; j++) {
            tr_i.data[i * tr_i.width + j] =  source.data[(source.height-1-j) * source.width + i];
        }
    }
    return tr_i;
}
